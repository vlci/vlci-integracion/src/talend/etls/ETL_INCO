package routines;
/*
import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPFile;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;
import it.sauronsoftware.ftp4j.FTPListParseException;
*/
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

/*
 * user specification: the function's comment should contain keys as follows: 1. write about the function's comment.but
 * it must be before the "{talendTypes}" key.
 * 
 * 2. {talendTypes} 's value must be talend Type, it is required . its value should be one of: String, char | Character,
 * long | Long, int | Integer, boolean | Boolean, byte | Byte, Date, double | Double, float | Float, Object, short |
 * Short
 * 
 * 3. {Category} define a category for the Function. it is required. its value is user-defined .
 * 
 * 4. {param} 's format is: {param} <type>[(<default value or closed list values>)] <name>[ : <comment>]
 * 
 * <type> 's value should be one of: string, int, list, double, object, boolean, long, char, date. <name>'s value is the
 * Function's parameter name. the {param} is optional. so if you the Function without the parameters. the {param} don't
 * added. you can have many parameters for the Function.
 * 
 * 5. {example} gives a example for the Function. it is optional.
 */
public class Utils {

	private static Properties props = null;

	public static String transformDateToString(Date fecha) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(fecha);

	}

	public static Date transformStringToDate(String fecha) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return format.parse(fecha);
		} catch (ParseException e) {
			return null;
		}

	}

	public static void loadProps(String fileAbsolutePath) throws IOException {
		File fichero = new File(fileAbsolutePath);

		if (!fichero.exists())
			fichero.createNewFile();

		InputStream configFile = new FileInputStream(fichero);
		props = new Properties();
		props.load(configFile);
		configFile.close();
	}

	public static void saveProps(String fileAbsolutePath) throws IOException {
		File fichero = new File(fileAbsolutePath);
		if (!fichero.exists())
			fichero.createNewFile();

		OutputStream configFile = new FileOutputStream(fichero);
		props.store(configFile, "");
		configFile.close();

	}

	public static String getProperty(String key, String fileAbsolutePath)
			throws IOException {
		if (props == null) {
			loadProps(fileAbsolutePath);
		}
		return props.getProperty(key);
	}

	public static void setPropertyDate(String key, String value,
			String fileAbsolutePath) throws IOException {
		if (props == null) {
			loadProps(fileAbsolutePath);
		}
		props.setProperty(key, value);
		saveProps(fileAbsolutePath);
	}

	public static Date getPropertyAsDate(String key, String fileAbsolutePath)
			throws IOException {
		if (props == null) {
			loadProps(fileAbsolutePath);
		}
		String value = props.getProperty(key);

		if ((value == null) || (value.isEmpty()))
			return null;

		return transformStringToDate(value);

	}

	public static void setPropertyDate(String key, Date value,
			String fileAbsolutePath) throws IOException {
		if (props == null) {
			loadProps(fileAbsolutePath);
		}
		props.setProperty(key, transformDateToString(value));
		saveProps(fileAbsolutePath);
	}

	public static Date createDate() {

		Calendar rightNow = Calendar.getInstance();

		rightNow.set(Calendar.DAY_OF_MONTH, 1);
		rightNow.set(Calendar.DAY_OF_YEAR, 1);
		rightNow.set(Calendar.HOUR_OF_DAY, 0);
		rightNow.set(Calendar.MINUTE, 0);
		rightNow.set(Calendar.SECOND, 0);
		rightNow.set(Calendar.MILLISECOND, 0);

		Date date = rightNow.getTime();

		return date;

	}

	public static Boolean esAnyoActual(String anyo) {

		Calendar rightNow = Calendar.getInstance();
		Integer anyoActual = rightNow.get(Calendar.YEAR);

		if (anyoActual.equals(Integer.valueOf(anyo))) {
			return true;
		}

		return false;
	}

	public static Date ParseFecha(String fecha) {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date fechaDate = null;
		try {
			fechaDate = formato.parse(fecha);
		} catch (ParseException ex) {
			System.out.println(ex);
		}
		return fechaDate;
	}

	public static String parseFecha(Date fecha) {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		String fechaDate = null;
		fechaDate = formato.format(fecha);
		return fechaDate;
	}

	public static String getNumberTwoDigits(Integer number) throws IOException {
		String numberTwoDigits = "";

		if (number != null) {
			if (number < 10)
				numberTwoDigits = "0" + number.toString();
			else
				numberTwoDigits = number.toString();
		}

		return numberTwoDigits;
	}

	public static Date getLastMonth() {

		Calendar cal = Calendar.getInstance();
		// cal.setTime(Calendar.getInstance().getTime());
		cal.add(Calendar.MONTH, -1);
		return cal.getTime();
	}

	public static int getNumDaysOfLastMonth() {
		Date date = getLastMonth();
		date = TalendDate.getLastDayOfMonth(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public static Date getLastDay() {

		Calendar cal = Calendar.getInstance();
		// cal.setTime(Calendar.getInstance().getTime());
		cal.add(Calendar.HOUR, -24);
		return cal.getTime();
	}

	public static String formatDouble(double num) {
		NumberFormat formatter = new DecimalFormat("#0.0000");
		return formatter.format(num).replace(",", ".");
	}

	public static boolean isFechaValida(String fecha) {
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = dateFormat.parse(fecha);
		} catch (ParseException e) {
			return false;
		}catch (NullPointerException e) {
			return false;
        }
		return true;
	}

	public static Date getPresentDay() {
		Calendar cal = Calendar.getInstance();
		// cal.setTime(Calendar.getInstance().getTime());
		// cal.add(Calendar.HOUR, -24);
		return cal.getTime();
	}

	// Para esta ETl es el dia actual menos 4
	public static Date getDataDay() {
		Calendar cal = Calendar.getInstance();
		// cal.setTime(Calendar.getInstance().getTime());
		cal.add(Calendar.HOUR, -96);
		return cal.getTime();
	}

	public static Date getDataRexecutionDay(String fechaIN) {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha;
		Calendar cal = Calendar.getInstance();
		try {
			fecha = formato.parse(fechaIN);
			cal.setTime(fecha);
			cal.add(Calendar.HOUR, -96);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cal.getTime();	
		
	}
	
	public static boolean isInteger(String s) {
		
		try{
			Integer.parseInt(s);
			// s is a valid integer
			return true;
		}catch (NumberFormatException ex){
			return false;
		}
	}
	
	public static boolean isDouble(String s) {
		try{
			Double num = Double.parseDouble(s)/100;
			return true;
		}catch (NumberFormatException ex){
			return false;
		}
	}
	
	public static boolean isDate(String s) {
		try {
			SimpleDateFormat formatoEntrada = new SimpleDateFormat("ddMMyyyy");
			formatoEntrada.setLenient(false);
			Date fecha = formatoEntrada.parse(s);
			return true;
		} catch (ParseException e) {
			return false;
		}catch (NullPointerException e) {
			return false;
        }
	}
	
	// Formatea String ddMMyyyy a un Date yyyy-MM-dd
	public static Date formatStringToDate(String s) {
	SimpleDateFormat formatoEntrada = new SimpleDateFormat("ddMMyyyy");
    
	    try {
	    	Date fecha = formatoEntrada.parse(s);
	        
	        return fecha;

	    } catch (ParseException e) {
	        return null;
	    }
	}
	
   public static String getSubstring(String cadena, int inicio, int fin) {
	   try {
		   return cadena.substring(inicio, fin);
	   } catch(IllegalArgumentException e) {
		   return null;
	   }
    }
}
